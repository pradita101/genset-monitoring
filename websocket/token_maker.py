#!/usr/bin/python3
# -*- coding: utf-8 -*-

import string
import random
import datetime,time
import sys, traceback
from authlib.jose import jwt
import bcrypt
import hashlib 
import json
import base64
from getpass import getpass



class TokenMaker():
    def __init__(self):        
        self.header = {'typ': 'JWT','alg': 'HS256'}
        self.payload = {}
        self.secretKey = ''
        self.expired = ''

    def generate_secret_key(self,char = 256):
        key = ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(char))
        return key
    
    def create_token(self, data, access):
        path = 'jwt-key.pub'
        data_user = data[0]
        days_file = open(path,'r')
        key = days_file.read()
        self.secretKey = key   
        self.payload['exp'] = round(time.time() + (86400 * 30))
        # self.payload['nbf'] = time.time() + 2
        self.payload['iss'] = 'genset_monitoring'
        self.payload['iat'] = datetime.datetime.utcnow()
        self.payload['uid'] = data_user['users_user_id']
        self.payload['uname'] =  data_user['users_username']
        self.payload['access'] =  access
        token = jwt.encode(self.header,self.payload,self.secretKey)        
        return token

    def generate_token(self, data, access):
        path = 'jwt-key.pub'
        data_user = data[0]
        days_file = open(path,'r')
        key = days_file.read()
        self.secretKey = key   
        self.payload['exp'] = round(time.time() + 86400)
        # self.payload['nbf'] = time.time() + 2
        self.payload['iss'] = 'genset_monitoring'
        self.payload['iat'] = datetime.datetime.utcnow()
        self.payload['uid'] = data_user['users_user_id']
        self.payload['uname'] =  data_user['users_username']
        self.payload['access'] =  access
        token = jwt.encode(self.header,self.payload,self.secretKey)        
        return token

    def get_user_data(self, token):
        userdata = {}
        # stringToken = token.split(".")
        path = 'jwt-key.pub'
        days_file = open(path,'r')
        key = days_file.read()
        user_json = jwt.decode(token, key)
        userdata['user_id'] = user_json['uid']
        userdata['username'] = user_json['uname']
        userdata['access'] = user_json['access']
        return userdata
        pass

    def check_token(self, token):
        path = 'jwt-key.pub'
        days_file = open(path,'r')
        key = days_file.read()
        claims = jwt.decode(token, key)
        if claims.validate() is None:
            return True

    def create_password(self,raw_password):
        result = hashlib.md5(raw_password.encode())
        return result.hexdigest()



if __name__ == "__main__":    
    try:
        tm = TokenMaker()
        password = "akulaku"
        secretkey = tm.generate_secret_key()
        print(type(secretkey))
        hash = tm.create_password(password)
        print(hash)
    except Exception as e:
        print(e)
        traceback.print_exc(file=sys.stdout)
        pass
