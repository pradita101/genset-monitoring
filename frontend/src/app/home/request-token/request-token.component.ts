import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { loginData } from "../../genset-data";
import { LoginService } from "../../login.service";
import { Router } from "@angular/router";
import { GeneralFunctionService } from "../../general-function.service";
import { Subscription } from 'rxjs';

@Component({
  selector: 'request-token',
  templateUrl: './request-token.component.html',
  styleUrls: ['./request-token.component.scss']
})
export class RequestTokenComponent implements OnInit, OnDestroy  {

  constructor( private login : LoginService,
    private router : Router,
    private fb: FormBuilder,
    private gf : GeneralFunctionService
   ) { }

   loginGroup : FormGroup

  dataLogin : loginData;
  status : Boolean;

  subscription : Subscription;
  token: string

  ngOnDestroy(){
    if(this.subscription){
      this.subscription.unsubscribe();
    }

  }


  ngOnInit() {
    this.gf.checkSession();
    this.token = ''


    this.loginGroup = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      access: ['API', Validators.required]
    });

  }

  requestToken(){
    this.subscription = this.login.getAccessToken(this.loginGroup.value).subscribe(result => this.getToken(result));
  }


  async getToken(data){
    this.token = data.token;
  }

}
